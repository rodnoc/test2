<?php
$allowed = array('main','auth','registration','chat');
if(!isset($_GET['page'])){
    $_GET['page']='main';
}
if(!in_array($_GET['page'],$allowed)){
    header("Location: /index.php?page=main");
    exit();
}

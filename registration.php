<?php
function registration($link){
	global $errors;
	$errors = array();
	if (isset($_POST['login'], $_POST['email'], $_POST['password'])) {
		if (empty($_POST['login'])) {
			$errors['login'] = 'Поле не заполнено';
		}
		if (empty($_POST['password'])) {
			$errors['password'] = 'Поле не заполнено';
		}
		if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = 'Поле не заполнено';
		}
		if (!count($errors)) {
			mysqli_query($link, "INSERT INTO `users` SET 
			`login`='" . mysqli_real_escape_string($link, $_POST['login']) . "',
			`password`='" . mysqli_real_escape_string($link, md5($_POST['password'])) . "',
			`email`='" . mysqli_real_escape_string($link, $_POST['email']) . "'
			");
			if (mysqli_errno($link)) {
				return $error_db = "Ошибка регистрации такой Логин или email существует";
			}
			else {$_SESSION['regok']='ok'; setcookie('name', $_POST['login'], time() + 9000);}
            //echo 'Ошибка ('.mysqli_errno($link).'): '. mysqli_error($link);
		}
	};
}
?>
<?php
$err_db=registration($link); ?>
<title>Чат-регистрация</title>
<div class="row h-100 justify-content-center align-items-center">
    <?php if(!@$_SESSION['regok']=='ok') { ?>
    <form action="" method="post" class="col-md-4">
		<h1 class="text-center">Чат-регистрация</h1>
		<div class="form-group">
            <h5 class="text-center">Логин:</h5>
            <input class="form-control" type="text" name="login" placeholder="Логин" value="<?php echo @htmlspecialchars($_POST['login']);?>">
			<span style="color:red"><?php echo @$errors['login']; ?></span>
        </div>
		<div class="form-group">
            <h5 class="text-center">Пароль:</h5>
            <input class="form-control" type="password" name="password" placeholder="Пароль" value="<?php echo @htmlspecialchars($_POST['password']);?>">
			<span style="color:red"><?php echo @$errors['password']; ?></span>
        </div>
        <div class="form-group">  
            <h5 class="text-center">Email:</h5>
            <input class="form-control" type="text" name="email" placeholder="Почта" value="<?php echo @htmlspecialchars($_POST['email']);?>">
            <span style="color:red"><?php echo @$errors['email'];echo $err_db; ?></span>
        </div>
		<div class="row h-10 justify-content-center align-items-center">
			<div class="col-md-1"><input class="btn btn-light btn-lg" type="submit" name="sendreg" value="OK"></div>
			<div class="col-md-1"></div>
			<button type="button" class="btn btn-primary btn-lg" onclick="self.location.href='index.php?page=main';">Отмена</button>
		</div>
	</form>
</div>
<?php } else { ?>
    <div class="row h-100 justify-content-center align-items-center"><h1 class="text-center">Вы успешно зарегестрировались!</h1></div>
    <?php echo("<meta http-equiv='refresh' content='1; URL=index.php?page=chat'>"); } ?>
    